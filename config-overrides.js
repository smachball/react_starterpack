const path = require("path");
const fs = require("fs");
const appDirectory = fs.realpathSync(process.cwd());

const {
  override,
  addDecoratorsLegacy,
  useBabelRc,
  addWebpackAlias,
} = require("customize-cra");

module.exports = override(
  addDecoratorsLegacy(),
  useBabelRc(),
  addWebpackAlias({
    ["components"]: path.resolve(__dirname, "src/components/"),
    ["assets"]: path.resolve(__dirname, "src/assets/"),
    ["views"]: path.resolve(__dirname, "src/views/"),
    ["themes"]: path.resolve(__dirname, "src/themes/"),
    ["lib"]: path.resolve(__dirname, "src/lib/"),
    ["config"]: path.resolve(__dirname, "src/config/"),

    // REDUX - API CALLS
    ["constants"]: path.resolve(__dirname, "src/redux/constants/"),
    ["reducers"]: path.resolve(__dirname, "src/redux/reducers/"),
    ["sagas"]: path.resolve(__dirname, "src/redux/sagas/"),
    ["actions"]: path.resolve(__dirname, "src/redux/actions/"),
    ["services"]: path.resolve(__dirname, "src/redux/services/"),
  })
);
