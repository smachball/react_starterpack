import React from "react";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Box,
  Typography,
  CardMedia,
} from "@mui/material";
import { notificationsActions } from "actions";
import { withStyles } from "@mui/styles";

import Logo from "assets/img/rfcs.png";

const styles = (theme) => ({
  root: {
    margin: 0,
  },
  btn: {
    width: "90%",
  },
});

@connect(({}) => ({}))
@withSnackbar
@withStyles(styles)
class HomePage extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(notificationsActions.success("Starter Pack Made by R@ymAn"));
    dispatch(notificationsActions.error("Starter Pack Made by R@ymAn"));
    dispatch(notificationsActions.warning("Starter Pack Made by R@ymAn"));
    dispatch(notificationsActions.info("Starter Pack Made by R@ymAn"));
  }

  render() {
    const { classes } = this.props;
    const handleNotif = (type) => {
      const { dispatch } = this.props;

      switch (type) {
        case "success":
          dispatch(notificationsActions.success("Wow !! Dispatch works !"));
          break;
        case "warning":
          dispatch(notificationsActions.warning("Wow !! Dispatch works !"));
          break;
        case "error":
          dispatch(notificationsActions.error("Wow !! Dispatch works !"));
          break;
        case "info":
          dispatch(notificationsActions.info("Wow !! Dispatch works !"));
          break;
        default:
          dispatch(notificationsActions.info("Wow !! Dispatch works !"));
          break;
      }
    };
    return (
      <>
        <Card square sx={{ position: "sticky", top: 0, p: 10 }}>
          <CardContent>
            <Box display="flex" justifyContent="center">
              <Typography
                align="center"
                variant="h1"
                sx={{
                  display: "inline-block",
                  fontWeight: 800,
                  color: "blue",
                  mr: 1,
                }}
              >
                StarterPack
              </Typography>
              <Typography
                align="center"
                variant="h1"
                sx={{
                  display: "inline-block",
                  fontWeight: 800,
                  mr: 1,
                  color: "white",
                }}
              >
                Made by
              </Typography>
              <Typography
                align="center"
                variant="h1"
                sx={{
                  display: "inline-block",
                  fontWeight: 800,
                  color: "red",
                }}
              >
                R@ymAn
              </Typography>
            </Box>
          </CardContent>
          <CardActions>
            <Grid container justifyContent="center">
              <Grid item md={1}>
                <Button
                  color="primary"
                  variant="contained"
                  className={classes.btn}
                  onClick={() => {
                    handleNotif("info");
                  }}
                >
                  INFO
                </Button>
              </Grid>
              <Grid item md={1}>
                <Button
                  color="error"
                  variant="contained"
                  className={classes.btn}
                  onClick={() => {
                    handleNotif("error");
                  }}
                >
                  Error
                </Button>
              </Grid>
              <Grid item md={1}>
                <Button
                  color="warning"
                  variant="contained"
                  className={classes.btn}
                  onClick={() => {
                    handleNotif("warning");
                  }}
                >
                  Warning
                </Button>
              </Grid>
              <Grid item md={1}>
                <Button
                  color="success"
                  variant="contained"
                  className={classes.btn}
                  onClick={() => {
                    handleNotif("success");
                  }}
                >
                  Success
                </Button>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
        <Box
          container
          display="flex"
          justifyContent="center"
          sx={{ my: "auto", mx: "auto", width: 800, height: 500 }}
        >
          <CardMedia component="img" alt="img" src={Logo} sx={{ width: 700 }} />
        </Box>
      </>
    );
  }
}

export default HomePage;
