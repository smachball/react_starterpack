import { notificationsConstants } from "constants";

export const notificationsActions = {
  enqueueSnackbar,
  closeSnackbar,
  removeSnackbar,
  success,
  error,
  warning,
  info,
};

function enqueueSnackbar(notification) {
  const key = notification.options && notification.options.key;

  return {
    type: notificationsConstants.ENQUEUE_SNACKBAR,
    notification: {
      ...notification,
      key: key || new Date().getTime() + Math.random(),
    },
  };
}

function closeSnackbar(key) {
  return {
    type: notificationsConstants.CLOSE_SNACKBAR,
    dismissAll: !key, // dismiss all if no key has been defined
    key,
  };
}

function removeSnackbar(key) {
  return {
    type: notificationsConstants.REMOVE_SNACKBAR,
    key,
  };
}

function success(message) {
  return enqueueSnackbar({
    message: message,
    options: {
      key: new Date().getTime() + Math.random(),
      variant: "success",
    },
  });
}

function error(message) {
  return enqueueSnackbar({
    message: message,
    options: {
      key: new Date().getTime() + Math.random(),
      variant: "error",
    },
  });
}

function warning(message) {
  return enqueueSnackbar({
    message: message,
    options: {
      key: new Date().getTime() + Math.random(),
      variant: "warning",
    },
  });
}

function info(message) {
  return enqueueSnackbar({
    message: message,
    options: {
      key: new Date().getTime() + Math.random(),
      variant: "info",
    },
  });
}
