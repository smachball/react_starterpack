import "./App.css";

// libs
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ThemeProvider, createTheme } from "@mui/material";
import useNotifier from "lib/useNotifier";

// routes
import HomePage from "views/HomePage";

//themes
import { themeOptions } from "themes/theme";

function App() {
  const theme = createTheme(themeOptions);
  useNotifier();

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Routes>
          <Route path="/" index element={<HomePage />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
