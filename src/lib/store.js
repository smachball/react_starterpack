import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "reducers";
import rootSaga from "sagas";
import { createLogger } from "redux-logger";

import { config } from "config";

let reducer = rootReducer;
let reduxMiddlewares = [];

if (config && config.ENV === `development`) {
  const logger = createLogger({
    predicate: true,
    collapsed: true,
  });
  reduxMiddlewares.push(logger);
}

const sagaMiddleware = createSagaMiddleware();
reduxMiddlewares.push(sagaMiddleware);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(...reduxMiddlewares))
);

sagaMiddleware.run(rootSaga);
