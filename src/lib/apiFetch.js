import { config } from "config";
// export const apiFetch = (route) => {
//   return fetch(`${API_URL}/${route}`).then((response) => {
//     return response.json();
//   });
// };

export const apiFetch = (path, options) => {
  return new Promise((resolve, reject) => {
    const url = [config.API_URL, path].join("");
    let fetchOptions = options ? { ...options } : {};
    let headers = fetchOptions.headers ? fetchOptions.headers : {};

    const user = localStorage.getItem("user");

    if (user && user.token) {
      headers["Authorization"] = "Bearer " + user.token;
    }
    if (headers["Access-Control-Request-Headers"]) {
      headers["Access-Control-Request-Headers"] += ", Authorization";
    } else {
      headers["Access-Control-Request-Headers"] = "Authorization";
    }

    fetchOptions.headers = {
      "Content-Type": "application/json",
      ...headers,
    };

    return fetch(url, fetchOptions)
      .then((response) => {
        if (!response.ok) {
          try {
            response
              .json()
              .then((data) =>
                reject({
                  ...data,
                  message:
                    data.message ||
                    data.error ||
                    (data.errors &&
                      Object.keys(data.errors)
                        .map((key) =>
                          key == "base"
                            ? data.errors[key]
                            : `${key}: ${data.errors[key]}`
                        )
                        .join(", ")),
                })
              )
              .catch((error) => reject({ message: response.statusText }));
          } catch (error) {
            reject(response);
          }
        } else if (fetchOptions.headers["Content-Type"] == "application/json") {
          try {
            resolve(response.json());
          } catch (error) {
            reject(error);
          }
        } else {
          resolve(response);
        }
      })
      .catch(reject);
  });
};
