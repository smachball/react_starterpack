import { createTheme, responsiveFontSizes } from "@mui/material/styles";
import { grey } from "@mui/material/colors";

export const themeOptions = responsiveFontSizes(
  createTheme({
    palette: {
      mode: "dark",
      background: {
        default: "#fff",
        paper: grey[900],
      },
    },
  })
);
