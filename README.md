# React_StarterPack

A React starter pack with :

- MUI
- Redux 
- Redux Saga
- Redux logger
- Notistack


# How to install

1 - First, u need to copy `config.dist.js` from `src/config/` to `config.js`

Modify `config.js` file with API_URL && ENV :

```
  API_URL: "http://localhost:3000/api/v1",
  ENV: "development",
```

2- Install yarn packages with `yarn install`

3- Start project with `yarn start`

4- Have fun !
